**SpringboardVR Modular Development**

The current structure of the system follows a modular approach. The following information should assist with development and best practices.

The main concept is that the system has various modules that make up the entire system. This will also allow for separation to microservices as a later date if required.

[TOC]

## Creating A New Module

Creating a module is simple and straightforward. Run the following command to create a module. Once you create the module, it will already be completely setup with the required ServiceProviders, routes files, and folder structure.

``` bash
php artisan module:make <module-name>
```

Replace `<module-name>` by your desired name.

It is also possible to create multiple modules in one command.

``` bash
php artisan module:make User Game Organization
```

## Working With Modules from Artisan

__Note all the following commands use "Game" as example module name, and example class/file names__

### Utility commands

**module:use**

Use a given module. This allows you to not specify the module name on other commands requiring the module name as an argument.

```bash
php artisan module:use Game
```

**module:unuse**

This unsets the specified module that was set with the `module:use` command.

```bash
php artisan module:unuse
```

**module:list**

List all available modules.

```bash
php artisan module:list
```

### TDD (Test Driven Development)

**module:make-test**

Generate the given test class for the specified module.

```bash
php artisan module:make-test EloquentGameRepositoryTest Game # Creates a laravel feature test
php artisan module:make-unit-test EloquentGameRepositoryUnitTest Game # To create a unit test
php artisan module:make-regression-test EloquentGameRepositoryRegressionTest Game # To create a unit test
```

**Important:** If this is a new module and you want to have tests working as part of the global scale test suite, you will need to add the following to the root `phpunit.xml` file inside of the `testsuites` element.

```xml
<testsuites>
    ...
    <testsuite name="[MODULE_NAME] Feature Tests">
        <directory suffix="Test.php">./Modules/[MODULE]/Tests/Feature</directory>
    </testsuite>
    <testsuite name="[MODULE_NAME] Regression Tests">
        <directory suffix="Test.php">./Modules/[MODULE]/Tests/Regression</directory>
    </testsuite>
    <testsuite name="[MODULE_NAME] Unit Tests">
        <directory suffix="Test.php">./Modules/[MODULE]/Tests/Unit</directory>
    </testsuite>
    ...
</testsuites>
```

### GraphQL Generation Commands

**module:make-graphql:enum**

Creates a new GraphQL Enum Class for the specified module. You will still need to create the assignment for it in the *Module/Config/graphql.php* configuration file.

```bash
php artisan module:make-graphql:enum SomeEnumClass Game
```

**module:make-graphql:field**

Creates a new GraphQL Field Class for the specified module. You will still need to create the assignment for it in the *Module/Config/graphql.php* configuration file.

```bash
php artisan module:make-graphql:field SomeFieldClass Game
```

**module:make-graphql:interface**

Creates a new GraphQL Interface Class for the specified module. You will still need to create the assignment for it in the *Module/Config/graphql.php* configuration file.

```bash
php artisan module:make-graphql:interface SomeInterfaceClass Game
```

**module:make-graphql:mutation**

Creates a new GraphQL Mutation Class for the specified module. You will still need to create the assignment for it in the *Module/Config/graphql.php* configuration file.

```bash
php artisan module:make-graphql:mutation SomeMutationClass Game
```

**module:make-graphql:query**

Creates a new GraphQL Query Class for the specified module. You will still need to create the assignment for it in the *Module/Config/graphql.php* configuration file.

```bash
php artisan module:make-graphql:query SomeQueryClass Game
```

**module:make-graphql:scalar**

Creates a new GraphQL Scalar Class for the specified module. You will still need to create the assignment for it in the *Module/Config/graphql.php* configuration file.

```bash
php artisan module:make-graphql:scalar SomeScalarClass Game
```

**module:make-graphql:type**

Creates a new GraphQL Type Class for the specified module. You will still need to create the assignment for it in the *Module/Config/graphql.php* configuration file.

```bash
php artisan module:make-graphql:type SomeTypeClass Game
```

### Additional Generation Commands

**module:make-command**

Generate the given console command for the specified module. Once you have created a command in a module it will automatically become available in artisan for use. **No additional registration required**.

```bash
php artisan module:make-command CreateGameCommand Game
```

**module:make-migration**

Generate a migration for specified module.

```bash
php artisan module:make-migration create_games_table Game
```

**module:make-seed**

Generate the given seed name for the specified module.

```bash
php artisan module:make-seed seed_fake_game_posts Game
```

**module:make-controller**

Generate a controller for the specified module.

```bash
php artisan module:make-controller GamesController Game
```

**module:make-model**

Generate the given model for the specified module.

```bash
php artisan module:make-model GameUser Game
```

Optional options:

- `--fillable=field1,field2`: set the fillable fields on the generated model
- `--migration`, `-m`: create the migration file for the given model

**module:make-provider**

Generate the given service provider name for the specified module.

```bash
php artisan module:make-provider GameServiceProvider Game
```

**module:make-middleware**

Generate the given middleware name for the specified module.

```bash
php artisan module:make-middleware CanReadGamesMiddleware Game
```

**module:make-mail**

Generate the given mail class for the specified module.

```bash
php artisan module:make-mail SendWeeklyGamesEmail Game
```

**module:make-notification**

Generate the given notification class name for the specified module.

```bash
php artisan module:make-notification NotifyAdminOfNewComment Game
```

**module:make-listener**

Generate the given listener for the specified module. Optionally you can specify which event class it should listen to. It also accepts a `--queued` flag allowed queued event listeners.

```bash
php artisan module:make-listener NotifyUsersOfANewGame Game
php artisan module:make-listener NotifyUsersOfANewGame Game --event=GameWasCreated
php artisan module:make-listener NotifyUsersOfANewGame Game --event=GameWasCreated --queued
```

**module:make-request**

Generate the given request for the specified module.

```bash
php artisan module:make-request CreateGameRequest Game
```

**module:make-event**

Generate the given event for the specified module.

```bash
php artisan module:make-event GameGameWasUpdated Game
```

**module:make-job**

Generate the given job for the specified module.

```bash
php artisan module:make-job JobName Game

php artisan module:make-job JobName Game --sync # A synchronous job class
```

**module:route-provider**

Generate the given route service provider for the specified module.

```bash
php artisan module:route-provider Game
```

**module:make-factory**

Generate the given database factory for the specified module.

```bash
php artisan module:make-factory FactoryName Game
```

**module:make-policy**

Generate the given policy class for the specified module.

The `Policies` is not generated by default when creating a new module. Change the value of `paths.generator.policies` in `modules.php` to your desired location.

```bash
php artisan module:make-policy PolicyName Game
```

**module:make-rule**

Generate the given validation rule class for the specified module.

The `Rules` folder is not generated by default when creating a new module. Change the value of `paths.generator.rules` in `modules.php` to your desired location.

```bash
php artisan module:make-rule ValidationRule Game
```

**module:make-resource**

Generate the given resource class for the specified module. It can have an optional `--collection` argument to generate a resource collection.

The `Transformers` folder is not generated by default when creating a new module. Change the value of `paths.generator.resource` in `modules.php` to your desired location.

```bash
php artisan module:make-resource GameResource Game
php artisan module:make-resource GameResource Game --collection
```

## Repositories

Inside of each Module, there is a Repositories folder. Since we are currently relying on Eloquent for our Data Service Layer, we will base our Repositories off of an **EloquentRepository** which already exists inside of the _Core_ Module.

### Create a New Repository

The basic setup for a repository is a simple two step process.

Creating a repository is very simple. For the purposes of this example. We are going to use the Entity (Model) Game. Generally a repository is just an extension of a Entity.

**STEP 1: Use Artisan to Generate Repository**

```bash
php artisan module:make-repository GameRepository
```

Inside of the Repositories Folder in the Module, you will notice the following structure:

```
Repositories/
  ├── Contracts/
      ├── GameRepositoryInterface.php
  ├── Eloquent/
      ├── GameRepository.php
```

**GameRepositoryInterface.php**

The contents of this file should be as follows:

```php
<?php

namespace SpringboardVr\Services\Game\Repositories\Contracts;

use SpringboardVr\Services\Core\Repositories\Contracts\RepositoryInterface;

interface GameRepositoryInterface extends RepositoryInterface
{
}
```

If you are going to be creating specific repository methods that would only be available for this repository, add the methods in this interface file first. This will ensure that we are coding to an interface (contract) to ensure cleaner code and flexibility. Currently we are creating an Eloquent based repository. However if we were to switch to Doctrine for example, your DoctrineGameRepository would implement this interface and force you to ensure that all methods are covered as required.

**GameRepository.php**

The contents of this file should be as follows:

```php
<?php

namespace SpringboardVr\Services\Game\Repositories\Eloquent;

use SpringboardVr\Services\Game\Entities\Game;
use SpringboardVr\Services\Core\Repositories\Eloquent\Repository;
use SpringboardVr\Services\Game\Repositories\Contracts\GameRepositoryInterface;

class GameRepository extends Repository implements GameRepositoryInterface
{
    /**
     * Returns the Entity Class Name
     *
     * @return mixed
     */
    protected function entity()
    {
        return Game::class;
    }
}
```

You will notice that this new class extends an abstract Repository Class located in the Core Module. The abstract class already contains all of the needed functionality to interact with your new repository, unless of course you have to add repository specific methods as you defined in your GameRepositoryInterface class file.

The main piece that makes this just work out of the box is that you need to ensure that the protected function `entity` returns the class name of your Entity (Model). This allows the repository to know what Entity we are going to be working with.

```
Repositories/
  ├── Contracts/
      ├── GameRepositoryInterface.php
  ├── Eloquent/
      ├── GameRepository.php
```

You are now ready to use your new Repository, however we still have one more step to complete.

**STEP 2: Register the Repository in the Module Service Provider**

If you move to the `Providers` Folder in your Module, open up the file that follows the naming convention of `<module-name>ServiceProvider`. In this case we would open `GameServiceProvider`.

```php
<?php

namespace SpringboardVr\Services\Game\Providers;

use SpringboardVr\Services\Core\Providers\AbstractModuleServiceProvider;

class GameServiceProvider extends AbstractModuleServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * The name of the current module
     * @var string
     */
    protected $moduleName = 'Game';

    /**
     * Middleware collection for module
     * @var array
     */
    protected $middleware = [];

    /**
     * Repositories collection for module. Broken down by driver => ['repo1', 'repo2', '...']
     * @var array
     */
    protected $repositories = [
        'Eloquent' => [
            'GameRepository'
        ]
    ];

    /**
     * Services collection for module.
     * @var array
     */
    protected $services = [];

    /**
     * Entity Observers collection for module.
     * @var array
     */
    protected $entityObservers = [];
}
```

You will notice that in this file there is a protected `$repositories` property. Since we are using Eloquent as our Driver, notice how simple this is, just add the filename of your Repository without the extension. In this example `GameRepository`. As you add more, you simply update this property.

The module service provider handles all of the heavy lifting for you by setting up the bindings within your application so that they can be included into your other files like controllers using DI (Dependancy Injection). However; remember that we are coding to an interface and we created that GameRepositoryInterface.php file.

When we want to make our repository accessible from within a controller for example, we actually want to call the interface file. This way if we ever decide to change from Eloquent to Doctrine, we only have to update our binding in the AbstractModuleServiceProvider class, not every controller.

```php
<?php

namespace SpringboardVr\Services\Game\Providers;

use SpringboardVr\Services\Game\Repositories\Contracts\GameRepositoryInterface;

class Controller {
  protected $gameRepository;

  public function __construct(GameRepositoryInterface $gameRepository) {
    $this->gameRepository = $gameRepository;
  }
}
```
Or if you want your code to be more readable and condensed, set the import of the interface to have a new name:

```php
<?php

namespace SpringboardVr\Services\Game\Providers;

use SpringboardVr\Services\Game\Repositories\Contracts\GameRepositoryInterface as GameRepository;

class Controller {
  protected $gameRepository;

  public function __construct(GameRepository $gameRepository) {
    $this->gameRepository = $gameRepository;
  }
}
```

### Working with Criteria

Criteria is similar to using Scopes in Eloquent. However, it allows each scope to become reusable elsewhere as it is stored within a class file. These criteria classes also follow the query builder format. The structure of a Criteria Class is as follows:

```php
<?php

namespace SpringboardVr\Services\Game\Repositories\Criteria;

use SpringboardVr\Services\Core\Repositories\Contracts\CriteriaInterface;
use SpringboardVr\Services\Core\Repositories\Contracts\RepositoryInterface;

class OrderByTitle implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $direction;

    /**
     * LimitToOrganizationsOfType constructor.
     *
     * @param string $direction
     */
    public function __construct($direction = 'asc')
    {
        $this->direction = $direction;
    }

    /**
     * Apply the criteria to the $entity query.
     *
     * @param $entity
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($entity, RepositoryInterface $repository)
    {
        return $entity->orderBy('title', $this->direction);
    }
}
```

Criteria Class files should be located inside the `Criteria` folder which resides inside the `Repositories` directory.

```
Repositories/
  ├── Contracts/
      ├── GameRepositoryInterface.php
  ├── Criteria/
      ├── OrderByTitle.php
  ├── Eloquent/
      ├── GameRepository.php
```

## Services

Inside of each Module, there is a Services folder. To ensure that we can build by extending base functionality, we will be extending the the Abstract Service Class from our Core Module, although this is not required in all cases.

### Create a New Service

The basic setup for a service is very similar to a repository. For the purposes of this example, we are going to work with the Game Module.

**STEP 1: Use Artisan to Generate Service**

```bash
php artisan module:make-service GameService
```

Inside of the Services Folder in the Module, you will notice the following structure:

```
Services/
  ├── Contracts/
      ├── GameServiceInterface.php
  ├── GameService.php
```

**GameServiceInterface.php**

The contents of this file should be as follows:

```php
<?php

namespace SpringboardVr\Services\Game\Services\Contracts;

use SpringboardVr\Services\Core\Services\Contracts\ServiceInterface;

interface GameServiceInterface extends ServiceInterface
{
}
```

If you are going to be creating specific service methods that would only be available for this service, add the methods in this interface file first. This will ensure that we are coding to an interface (contract) to ensure cleaner code and flexibility.

**GameService.php**

The contents of this file should be as follows:

```php
<?php

namespace SpringboardVr\Services\Game\Services;

use SpringboardVr\Services\Core\Services\Service;
use SpringboardVr\Services\Game\Services\Contracts\GameServiceInterface;

class GameService extends Service implements GameServiceInterface
{
}
```

You will notice that this new class extends an abstract Service Class located in the Core Module. The abstract class already contains all of the needed functionality to interact with your new service, unless of course you have to add service specific methods as you defined in your GameServiceInterface class file.

You are now ready to use your new Service, however we still have one more step to complete.

**STEP 2: Register the Service in the Module Service Provider**

If you move to the `Providers` Folder in your Module, open up the file that follows the naming convention of `<module-name>ServiceProvider`. In this case we would open `GameServiceProvider`.

```php
<?php

namespace SpringboardVr\Services\Game\Providers;

use SpringboardVr\Services\Core\Providers\AbstractModuleServiceProvider;

class GameServiceProvider extends AbstractModuleServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * The name of the current module
     * @var string
     */
    protected $moduleName = 'Game';

    /**
     * Middleware collection for module
     * @var array
     */
    protected $middleware = [];

    /**
     * Repositories collection for module. Broken down by driver => ['repo1', 'repo2', '...']
     * @var array
     */
    protected $repositories = [
        'Eloquent' => [
            'GameRepository'
        ]
    ];

    /**
     * Services collection for module.
     * @var array
     */
    protected $services = [
      'GameService'
    ];

    /**
     * Entity Observers collection for module.
     * @var array
     */
    protected $entityObservers = [];
}
```

You will notice that in this file there is a protected `$services` property. Notice how simple this is, just add the filename of your Service without the extension. In this example `GameService`. As you add more, you simply update this property.

The module service provider handles all of the heavy lifting for you by setting up the bindings within your application so that they can be included into your other files like controllers using DI (Dependancy Injection). However; remember that we are coding to an interface and we created that GameServiceInterface.php file.

When we want to make our service accessible from within a controller for example, we actually want to call the interface file.

```php
<?php

namespace SpringboardVr\Services\Game\Providers;

use SpringboardVr\Services\Game\Services\Contracts\GameServiceInterface;

class Controller {
  protected $gameService;

  public function __construct(GameServiceInterface $gameService) {
    $this->gameService = $gameService;
  }
}
```
Or if you want your code to be more readable and condensed, set the import of the interface to have a new name:

```php
<?php

namespace SpringboardVr\Services\Game\Providers;

use SpringboardVr\Services\Game\Services\Contracts\GameServiceInterface as GameService;

class Controller {
  protected $gameService;

  public function __construct(GameService $gameService) {
    $this->gameService = $gameService;
  }
}
```
