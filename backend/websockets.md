# Websockets

## Overview
Our websocket implementation is currently in a state of flux. This is an onverview

Right now (January 2019) we are using the [tlaverdure/laravel-echo-server](https://github.com/tlaverdure/laravel-echo-server) package. It is pretty awful, doesn't scale well, has zero documentation, and is slow.

In early 2019 we are planning on adding support for the [beyondcode/laravel-websockets](https://github.com/beyondcode/laravel-websockets) server. Initially we will be broadcasting on both services until we migrate all of our frontend and desktop servers off of it. It is Pusher compliant and is well documented which puts us in a much better position for the future.

Once we have moved over to beyondcode/laravel-websockets and have updated our [GraphQL](graphql.md) implementation to be using the lighthouse package we will be looking at adding support for [GraphQL Subscriptions](https://graphql.org/blog/subscriptions-in-graphql-and-relay/) and slowly deprecate our existing method of websockets.

## Technical
We offer a variety of private channels depending on what your use case is. The primary ones are as follows:
 * App.Station.{UUID}
 * App.Location.{UUID}
 * App.Organization.{UUID}

They should all be pretty self explanatory and are used to limit the scope of what events you want to know about.

Once subscribed events will be broadcasted as the singular name of the models. Here is an exhaustive list of possible events
 * Station - Organization, Location, Station
 * Game - Organization, Location, Station
 * GameLaunch - Station
 * GameOwnership - Organization
 * Booking - Organization, Location, Station
 * BookingStationTime - Organization, Location, Station
 * Experience - Organization, Location, Station
 * Location - Organization, Location, Station
 * Organization - Organization, Location, Station
 * StationGame - Station
 * StationGameLaunch - Station

Each of these events is the entire model in JSON format. All key values are camelCase and all ID values are in UUID (where available).
