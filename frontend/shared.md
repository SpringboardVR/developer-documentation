# SpringboardVR Shared Frontend Resources

SpringboardVR has a library of shared resources that we use in every frontend application. It includes:

* CSS variables and property sets
* Common set up scripts
* Auth components, Auth vuex modules and auth routes
* i18n translations
* and more!

Make sure that if there is code you would like to re-use across application, that you put that into here.

I will make these docs better soon.
