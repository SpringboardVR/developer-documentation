# Setting up a new App

Using our Kubernetes cluster requires a few small tasks within Gitlab to make work. This assumes you've already got a repo created in Gitlab.

#### Step 1: Make Namespace & Create Cluster Role Binding
Ask someone with `kubectl` to create the namespace you want to use. For example

```
kubectl create namespace SOME_NAMESPACE_NAME
```

Then ask that same person to create `clusterrolebinding` like this. Replacing [NAMESPACE] with the namespace.

```
kubectl create clusterrolebinding [NAMESPACE]-admin-binding --clusterrole=cluster-admin --user=system:serviceaccount:[NAMESPACE]:[NAMESPACE]-service-account
```

#### Step 2: Let Gitlab Know About Kube
We then need to tell Gitlab about our Kubernetes cluster. Go to the Kubernetes section of your Gitlab repo. And hit "Add Existing Cluster".

From there you'll want to go to the same section of another SpringboardVR Gitlab repo that already has a Kubernetes cluster setup. Go to the cluster details and copy all the information from the existing repo's cluster info into your new repo's cluster information.

![Kubernetes Cluster Settings in Gitlab](./images/kube-gitlab.png>)

#### Step 3. Get Necessary Boilerplate
You will need 3 things in your repo to make the Devops pipelines work:

##### First
A `Dockerfile` at project root. This Dockerfile will be what is build during the build step of the Devops Pipeline. You can...

- If you're familiar with Docker write one you know will work.
- Copy paste from a similar app in SpringboardVR's Gitlab.
- Ask for help.

##### Second
A `.gitlab-ci.yml` file at project root. Every one of our apps follows a template so you just need to copy and paste the following into the file and replace some values
```
include: 'https://gitlab.com/SpringboardVR/gitlab-ci-templates/raw/master/standard-template.yml'

variables:
  APPLICATION_PREFIX: [URL PREFIX YOU WANT]
  NAMESPACE: [KUBERNETES NAMESPACE (SAME AS STEP 1)]
  HELM_CHART: springboardvr/[YOUR CHOICE OF laravel/go/frontend, ask if you don't know what to put here]
```

> A Note On Testing... This wont enable the ability to do unit testing so ask for help to set up unit tests, or look at other SpringboardVR repos to see examples of how to setup testing in this file.

##### Third
a directory named `.devops` that contains value files for different server environments. In that directory make three files.

```
.devops/production-values.yaml
.devops/staging-values.yaml
.devops/review-values.yaml
```

These set values for the environments that are automatically setup in our Gitlab CI deployment pipeline. The entries that go in these files are tied the `HELM_CHART` value we selected in the `.gitlab-ci.yml` file. For filling out these files you can...

- If you're familiar with Helm, you can look at the infrastructure repo to figure out what values you need to override in these files.
- See examples from other SpringboardVR repos that are using the same `HELM_CHART`
- Ask for help.

#### Success!

Now when you push the GitlabCI should build and deploy automatically! For `feature/*` branches it will create a review app, for master it will deploy to staging automatically, from there you can push to production from the Gitlab web ui.

