# SpringboardVR

* [Introduction](readme.md)
* [Projects](projects.md)

## Docker Setup
* [MacOS](docker/mac.md)
* [Windows](docker/windows.md)
* [Linux](docker/linux.md)

## Backend PHP
* [Working With Modules](backend/modules.md)
* [Authentication](backend/auth.md)
* [Microservices](backend/microservices.md)
* [GraphQL](backend/graphql.md)
* [Websockets](backend/websockets.md)

## Backend Go
* Nothing Yet

## Desktop Go
* Nothing Yet

## Desktop C&#35;
* Nothing Yet

## Frontend
* [Working With Shared Resources](frontend/shared.md)

## DevOps
* [Overview](devops/overview.md)
* [Troubleshooting](devops/troubleshooting.md)
* [Setting Up a New App](devops/setting-up-new-app.md)

## GDPR
* [Data Breach Policy](GDPR/data-breach-policy.md)

