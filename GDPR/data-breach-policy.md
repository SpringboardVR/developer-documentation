Data Breach Policy
==================

**Introduction**
----------------

This Policy and Plan aims to help SpringboardVR manage personal data breaches effectively. 

SpringboardVR holds Personal Data about our users, employees, clients, suppliers and other individuals for a variety of business purposes. 

SpringboardVR is committed not only to the letter of the law but also to the spirit of the law and places a high premium on the correct, lawful and fair handling of all Personal Data, respecting the legal rights, privacy and trust of all individuals with whom it deals. 

A data breach generally refers to the unauthorised access and retrieval of information that may include corporate and / or personal data.  Data breaches are generally recognised as one of the more costly security failures of organisations. They could lead to financial losses, and cause consumers to lose trust in SpringboardVR or our clients. 

The regulations across the various jurisdictions in which SpringboardVR operates require SpringboardVR to make reasonable security arrangements to protect the personal data that we possess or control, to prevent unauthorised access, collection, use, disclosure, or similar risks. 

**Scope**
---------

This policy applies to all staff.  You must be familiar with this policy and comply with its terms.  This policy supplements our other policies relating to internet and email use.  We may supplement or amend this policy by additional policies and guidelines from time to time.  Any new or modified policy will be circulated to staff before being adopted. 

As our Data Protection Officer, Matthew Hall has overall responsibility for the day-to-day implementation of this policy. 

**Training**
------------

All staff will receive training on this policy. New staff will receive training as part of the induction process. Further training will be provided at least every year or whenever there is a substantial change in the law or our policy and procedure. 

Training is provided through an in-house seminar and online training on an annual basis, and covers the applicable laws relating to data protection, and SpringboardVR’s data protection and related policies and procedures. 

Completion of training is compulsory. 

If you have any questions or concerns about anything in this policy, do not hesitate to contact the DPO. 

**Applicable Legislation Considerations**

--------------------------------------------

### UK Data Protection Act 1998 (DPA)

Under the UK Data Protection Act 1998, Personal Data is defined as data which relates to a living individual who can be identified from that data, or from that data and other information which is in the possession of, or is likely to come into the possession of, the data controller, and includes any expression of opinion about the individual and any indication of the intentions of the data controller or any other person in respect of the individual. 

The UK Data Protection Act 1998 also defines “**sensitive Personal Data**” as Personal Data relating to the racial or ethnic origin of the data subject; their political opinions; their religious (or similar) beliefs; trade union membership;** their physical or mental health condition**; their sexual life; the commission or alleged commission by them of any offence; or any proceedings for any offence committed or alleged to have been committed by them, the disposal of such proceedings or the sentence of any court in such proceedings.  

### Singapore PDPA (PDPA)

Personal Data is defined in the PDPA as “data, whether true or not, about an individual who can be identified a) from that data; or b) from that data and other information to which the organisation has or is likely to have access.” 

### EU General Data Protection Regulation (EU) 2016/679 (GDPR)

The regulation applies if the data controller (organization that collects data from EU residents) or processor (organization that processes data on behalf of data controller e.g. cloud service providers) or the data subject (person) is based in the EU. 

Regulation also applies to organizations based outside the European Union if they collect or process personal data of EU residents. 

According to the European Commission, Personal Data is: "any information relating to an individual, whether it relates to his or her private, professional or public life. It can be anything from a name, a home address, a photo, an email address, bank details, posts on social networking websites, medical information, or a computer’s IP address." 

**Personal Data**
-----------------

SpringboardVR defines Personal Data as the broader of the definitions contained in the PDPA, DPA, and GDPR.

SpringboardVR defines Sensitive Personal Data as the broader of the definitions contained in the PDPA, DPA, and GDPR.

Any use of sensitive Personal Data is to be strictly controlled in accordance with this policy.

While some data will always relate to an individual, other data may not, on its own, relate to an individual. Such data would not constitute Personal Data unless it is associated with, or made to relate to, a particular individual.

For the purposes of the **Singaporean PDPA**, SpringboardVR is a Data Intermediary.  From the Act:
“data intermediary” is an organisation which processes Personal Data on behalf of another organisation but does not include an employee of that other organisation.

Generic information that does not relate to a particular individual may also form part of an individual’s Personal Data when combined with Personal Data or other information to enable an individual to be identified.

Aggregated data is _not_ Personal Data.

SpringboardVR gathers Personal Data for two purposes: for _virtual reality management_, and for _internal operations_.

Personal Data for _virtual reality management_ relates to identifiable individual users and may include:

·       user profile information such as Full name, Photograph, Date of Birth, Mobile telephone number, and Personal email address;

·       game playing information and statistics;

Personal Data we gather for _internal operational_ purposes relates to identifiable individuals such as job applicants, current and former employees, contract and other staff, clients, suppliers, and marketing contacts, and the data gathered may include individuals' contact details, educational background, financial and pay details, details of certificates and diplomas, education and skills, marital status, nationality, job title, and CV.

Causes
------

Data breaches may be caused by employees, parties external to the organisation, or computer system errors. 

**Human Error**
Human Error causes include: 

*   Loss of computing devices (portable or otherwise), data storage devices, or paper records containing personal data 
*   Disclosing data to a wrong recipient 
*   Handling data in an unauthorised way (eg: downloading a local copy of personal data) 
*   Unauthorised access or disclosure of personal data by employees (eg: sharing a login) 
*   Improper disposal of personal data (eg: hard disk, storage media, or paper documents containing personal data sold or discarded before data is properly deleted) 

**Malicious Activities**
Malicious causes include: 

*   Hacking incidents / Illegal access to databases containing personal data 
*   Hacking to access unauthorised data via the Operator panel, Developer panel, Customer panel, Monitor application, Desktop Client, or API 
*   Theft of computing devices (portable or otherwise), data storage devices, or paper records containing personal data 
*   Scams that trick SpringboardVR staff into releasing personal data of individuals 

**Computer System Error**
Computer System Error causes include 

*   Errors or bugs in SpringboardVR’s Operator panel, Developer panel, Customer panel, Monitor application, Desktop Client, or API 
*   Failure of cloud services (eg: Gitlab), cloud computing (eg: Amazon Web Services) or cloud storage (eg: Google Drive) security / authentication / authorisation systems 

**Reporting Breaches**
----------------------

All members of staff have an obligation to report actual or potential data protection compliance failures. This allows us to: 

*   Investigate the failure and take remedial steps if necessary 
*   Maintain a register of compliance failures 
*   Notify the Supervisory Authority of any compliance failures that are material either in their own right or as part of a pattern of failures 

Under the GDPR, the DPO is legally obliged to notify the Supervisory Authority within 72 hours of the data breach (Article 33). Individuals have to be notified if adverse impact is determined (Article 34). In addition, SpringboardVR must notify any affected clients without undue delay after becoming aware of a personal data breach (Article 33). 

However, SpringboardVR does not have to notify the data subjects if anonymized data is breached. Specifically, the notice to data subjects is not required if the data controller has implemented pseudonymisation techniques like encryption along with adequate technical and organizational protection measures to the personal data affected by the data breach (Article 34). 

**Data Breach Team**
--------------------

The Data Breach Team consists of the DPO.

The Data Breach Team should immediately be alerted of any confirmed or suspected data breach via mobile phone: 

*   Matthew Hall (DPO): +1 604 401 2808
*   Email: [matthew@springboardvr.com](mailto:matthew@springboardvr.com)

### Reporting the Incident to the Personal Data Protection Commission

In the case where affected individuals are in the EU, the relevant supervisory authority must be notified as soon as possible of any data breaches that might cause public concern or where there is a risk of harm to a group of affected individuals.  (Each EU state has its own supervisory authority.) 

In the case where affected individuals are in Singapore, the PDPC must be notified as soon as possible using info@pdpc.gov.sg with the e-mail subject “\[Data Breach Notification\]”.  For urgent notification of major cases, organisations may also contact the Commission at +65 6377 3131. 

In either case, the notification should include the following information, where available: 

*   Extent of the data breach 
*   Type and volume of personal data involved 
*   Cause or suspected cause of the breach 
*   Whether the breach has been rectified 
*   Measures and processes that the organisation had put in place at the time of the breach 
*   Information on whether affected individuals of the data breach were notified and if not, when the organisation intends to do so 
*   Contact details of SpringboardVR staff with whom the supervisory authority can liaise for further information or clarification 

Where specific information of the data breach is not yet available, SpringboardVR should send an interim notification comprising a brief description of the incident. 

Notifications made by organisations or the lack of notification, as well as whether organisations have adequate recovery procedures in place, will affect supervising authorities’ decision(s) on whether an organisation has reasonably protected the personal data under its control or possession. 

**Responding to a Data Breach**
-------------------------------

### Data Breach Management Plan

Upon being notified of a (suspected or confirmed) data breach, the Data Breach Team should immediately activate the data breach & response plan. 

SpringboardVR’s data breach management and response plan is: 

1.  Confirm the Breach 
2.  Contain the Breach 
3.  Assess Risks and Impact 
4.  Report the Incident 
5.  Evaluate the Response & Recovery to Prevent Future Breaches

### 1\. Confirm the Breach

The Data Breach Team (DBT) should act as soon as it is aware of a data breach.  Where possible, it should first confirm that the data breach has occurred.  It may make sense for the DBT to proceed Contain the Breach on the basis of an unconfirmed reported data breach, depending on the likelihood of the severity of risk. 

### 2\. Contain the Breach

The DBT should consider the following measures to Contain the Breach, where applicable: 

*   Shut down the compromised system that led to the data breach. 
*   Establish whether steps can be taken to recover lost data and limit any damage caused by the breach. 
*   (eg: remotely disabling / wiping a lost notebook containing personal data of individuals.) 
*   Prevent further unauthorised access to the system. 
*   Reset passwords if accounts and / or passwords have been compromised. 
*   Isolate the causes of the data breach in the system, and where applicable, change the access rights to the compromised system and remove external connections to the system. 

### 3\. Assess Risks and Impact 

Knowing the risks and impact of data breaches will help SpringboardVR determine whether there could be serious consequences to affected individuals, as well as the steps necessary to notify the individuals affected. 

**_Risk and Impact on Individuals_**

*   How many people were affected? 
    A higher number may not mean a higher risk, but assessing this helps overall risk assessment. 
*   Whose personal data had been breached? 
    Does the personal data belong to employees, customers, or minors? Different people will face varying levels of risk as a result of a loss of personal data. 
*   What types of personal data were involved? 
    This will help to ascertain if there are risk to reputation, identity theft, safety and/or financial loss of affected individuals. 
*   Any additional measures in place to minimise the impact of a data breach? 
    Eg: a lost device protected by a strong password or encryption could reduce the impact of a data breach. 

**_Risk and Impact on Organisations_**

*   What caused the data breach? 
    Determining how the breach occurred (through theft, accident, unauthorised access, etc.) will help identify immediate steps to take to contain the breach and restore public confidence in a product or service. 
*   When and how often did the breach occur? 
    Examining this will help SpringboardVR better understand the nature of the breach (e.g. malicious or accidental). 
*   Who might gain access to the compromised personal data? 
    This will ascertain how the compromised data could be used. In particular, affected individuals must be notified if personal data is acquired by an unauthorised person. 
*   Will compromised data affect transactions with any other third parties? 
    Determining this will help identify if other organisations need to be notified. 

### 4\. Report the Incident 

SpringboardVR is legally required to notify affected individuals if their personal data has been breached.  This will encourage individuals to take preventive measures to reduce the impact of the data breach, and also help SpringboardVR rebuild consumer trust. 

_**Who to Notify: **_

*   Notify individuals whose personal data have been compromised. 
*   Notify other third parties such as banks, credit card companies or the police, where relevant. 
*   Notify PDPC / GDPR especially if a data breach involves sensitive personal data. 
*   The relevant authorities (eg: police) should be notified if criminal activity is suspected and evidence for investigation should be preserved (eg: hacking, theft or unauthorised system access by an employee.) 

_**When to Notify: **_

*   Notify affected individuals immediately if a data breach involves sensitive personal data. This allows them to take necessary actions early to avoid potential abuse of the compromised data. 
*   Notify affected individuals when the data breach is resolved 

_**How to Notify: **_

*   Use the most effective ways to reach out to affected individuals, taking into consideration the urgency of the situation and number of individuals affected (e.g. media releases, social media, mobile messaging, SMS, e-mails, telephone calls). 
*   Notifications should be simple to understand, specific, and provide clear instructions on what individuals can do to protect themselves. 

_**What to Notify: **_

How and when the data breach occurred, and the types of personal data involved in the data breach. 

*   What SpringboardVR has done or will be doing in response to the risks brought about by the data breach. 
*   Specific facts on the data breach where applicable, and actions individuals can take to prevent that data from being misused or abused. 
*   Contact details and how affected individuals can reach the organisation for further information or assistance (e.g. helpline numbers, e-mail addresses or website). 

### 5\. Evaluate the Response & Recovery to Prevent Future Breaches 

After steps have been taken to resolve the data breach, SpringboardVR should review the cause of the breach and evaluate if existing protection and prevention measures and processes are sufficient to prevent similar breaches from occurring, and where applicable put a stop to practices which led to the data breach. 

_**Operational and Policy Related Issues: **_

*   Were audits regularly conducted on both physical and IT-related security measures? 
*   Are there processes that can be streamlined or introduced to limit the damage if future breaches happen or to prevent a relapse? 
*   Were there weaknesses in existing security measures such as the use of outdated software and protection measures, or weaknesses in the use of portable storage devices, networking, or connectivity to the Internet? 
*   Were the methods for accessing and transmitting personal data sufficiently secure, eg: access limited to authorised personnel only? 
*   Should support services from external parties be enhanced, such as vendors and partners, to better protect personal data? 
*   Were the responsibilities of vendors and partners clearly defined in relation to the handling of personal data? 
*   Is there a need to develop new data-breach scenarios? 

_**Resource Related Issues: **_
Were sufficient resources allocated to manage the data breach? 

*   Should external resources be engaged to better manage such incidents? 
*   Were key personnel given sufficient resources to manage the incident? 

_**Employee Related Issues: **_

*   Were employees aware of security related issues? 
*   Was training provided on personal data protection matters and incident management skills? 
*   Were employees informed of the data breach and the learning points from the incident? 

_**Management Related Issues: **_

*   How was management involved in the management of the data breach? 
*   Was there a clear line of responsibility and communication during the management of the data breach? 

**Monitoring**
--------------

Everyone must observe this policy. 

The DPO has overall responsibility for this policy. 

The DPO will review and monitor this policy regularly to make sure it is effective, relevant, and adhered to. 

**Consequences of failing to comply**
-------------------------------------

We take compliance with this policy very seriously. Failure to comply puts both you and the organisation at risk. 

The importance of this policy means that failure to comply with any requirement may lead to disciplinary action under our procedures which may result in dismissal.
