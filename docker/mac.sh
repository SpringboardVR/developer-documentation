#!/bin/bash

mkdir springboardvr;
cd springboardvr;
springboardVRCodeDirectory=$(pwd);

git clone --recurse-submodules git@gitlab.com:SpringboardVR/api.git;
git clone --recurse-submodules git@gitlab.com:SpringboardVR/public-api.git;
git clone --recurse-submodules git@gitlab.com:SpringboardVR/build.git build-api;

git clone --recurse-submodules git@gitlab.com:SpringboardVR/monitor.git;
git clone --recurse-submodules git@gitlab.com:SpringboardVR/operator-panel.git;
git clone --recurse-submodules git@gitlab.com:SpringboardVR/developer-panel.git;
git clone --recurse-submodules git@gitlab.com:SpringboardVR/customer-panel.git;
git clone --recurse-submodules git@gitlab.com:SpringboardVR/station.git;
git clone --recurse-submodules git@gitlab.com:SpringboardVR/public-ssr.git;

git clone --recurse-submodules git@gitlab.com:SpringboardVR/content-distribution.git;

cd api;
docker run -it -v $(pwd):/var/www/html sbvr/composer-node /bin/sh -c "yarn && bower install --allow-root";
docker-compose up -d;
sleep 10;
docker-compose exec utility composer install;
docker-compose exec utility php artisan migrate;
docker-compose exec utility php artisan db:seed --class=FeatureBranchSeeder;
docker-compose stop;
cd ..;

cd public-api;
docker-compose up -d;
sleep 10;
docker-compose exec utility composer install;
docker-compose exec utility php artisan migrate;
docker-compose stop;
cd ..;

cd build-api;
docker-compose up -d;
sleep 10;
docker-compose exec utility composer install;
docker-compose exec utility php artisan migrate;
docker-compose stop;
cd ..;

cd monitor;
docker run -it -v $(pwd):/usr/share/nginx/html sbvr/nginx-node /bin/sh -c "yarn";
cd ..;
cd operator-panel;
docker run -it -v $(pwd):/usr/share/nginx/html sbvr/nginx-node /bin/sh -c "yarn";
cd ..;
cd developer-panel;
docker run -it -v $(pwd):/usr/share/nginx/html sbvr/nginx-node /bin/sh -c "yarn";
cd ..;
cd customer-panel;
docker run -it -v $(pwd):/usr/share/nginx/html sbvr/nginx-node /bin/sh -c "yarn";
cd ..;
cd station;
docker run -it -v $(pwd):/usr/share/nginx/html sbvr/nginx-node /bin/sh -c "yarn";
cd ..;
cd public-ssr;
docker run -it -v $(pwd):/usr/share/nginx/html sbvr/nginx-node /bin/sh -c "yarn";
cd ..;

mkdir -p ~/.dinghy/certs;

cd ~/.dinghy/certs;

openssl req -x509 -newkey rsa:2048 -keyout app.springboardvr.docker.key \
-out app.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=app.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:app.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain app.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout api.springboardvr.docker.key \
-out api.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=api.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:api.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain api.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout graphql.springboardvr.docker.key \
-out graphql.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=graphql.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:graphql.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain graphql.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout socket.springboardvr.docker.key \
-out socket.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=socket.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:socket.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain socket.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout public-api.springboardvr.docker.key \
-out public-api.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=public-api.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:public-api.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain public-api.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout builds.springboardvr.docker.key \
-out builds.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=builds.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:builds.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain builds.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout monitor.springboardvr.docker.key \
-out monitor.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=monitor.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:monitor.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain monitor.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout operator.springboardvr.docker.key \
-out operator.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=operator.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:operator.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain operator.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout developer.springboardvr.docker.key \
-out developer.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=developer.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:developer.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain developer.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout customer.springboardvr.docker.key \
-out customer.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=customer.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:customer.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain customer.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout booking.springboardvr.docker.key \
-out booking.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=booking.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:booking.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain booking.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout station.springboardvr.docker.key \
-out station.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=station.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:station.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain station.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout content-disribution.springboardvr.docker.key \
-out content-disribution.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=content-disribution.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:content-disribution.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain content-disribution.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout public-ssr.springboardvr.docker.key \
-out public-ssr.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=public-ssr.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:public-ssr.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain public-ssr.springboardvr.docker.crt;

openssl req -x509 -newkey rsa:2048 -keyout content-distribution.springboardvr.docker.key \
-out content-distribution.springboardvr.docker.crt -days 365 -nodes \
-subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=content-distribution.springboardvr.docker" \
-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:content-distribution.springboardvr.docker")) \
-reqexts SAN -extensions SAN;
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain content-distribution.springboardvr.docker.crt;

cd $springboardVRCodeDirectory;
