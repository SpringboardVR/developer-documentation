# MacOS Setup Guide for Docker

This document will help you get setup in an up to date MacOS environment.

As a prerequisite You must have a `gitlab` account that has access to SpringboardVR's `gitlab` group.

## Step 1: Get the Tools

If you haven't already be sure to get `brew` on your machine. You can find instructions on how to do that [here](https://brew.sh/).

Our apps rely heavily on `docker` for running our apps both locally and on server. So, we'll need to install a way to run `docker` containers on our machine. We prefer a tool called Dinghy, it's faster and has more features than `Docker for Mac`. To use it we first need `virtualbox` and it's extension back. Let's get those with `brew`.

```bash
brew cask install virtualbox virtualbox-extension-pack
```

Now we're ready to install `dinghy`, follow the install steps [here](https://github.com/codekitchen/dinghy).

Once `dinghy` is fully up and running we will need two more tools.

```bash
brew install docker-compose openssl
```

`docker-compose` makes it easier for us to run applications that rely on more than one `docker` container.
`openssl` gives us better control over the creation of self signed certificates, which we use to HTTPS enable our local development.

## Step 2: Get the Code

Download [this script file](./mac.sh) and move it to a folder which will contain all of your SpringboardVR code. The only requirement is that it must be under `~`. For example `~/code/`. Once the script is in the file navigate to that folder in your terminal and run.

```bash
chmod +x mac.sh && ./mac.sh
```

This command will ask for you for your password once it starts generating certificates.

> Note: What does this script do?
> 1. `git clone` all applications.
> 2. Download every projects individual dependencies.
> 3. Create SSL certificates for all URLs.

## Step 3: Run the Apps

> NOTE: ENV files
> Several of our applications require an `.env` file to be in the root of the application folder. We don't version control this because there is sensitive data contained within these files. Be sure to ask an existing dev for that file, or you can infer the values from the `.env.example` if it exists.

We use `docker-compose` to boot up the applications. This tool will look for a `docker-compose.yaml` in the current directory and run according to settings in that file. Once running it will attach itself to your terminal window and print logs, so be prepared to make a few terminal tabs.

### Running an App

As a general rule of thumb, even if you're not working on it the API will always need to be running when you're developing for web. Lets boot that up first. Navigate to the `api` folder we made earlier and run the following command.

```bash
docker-compose up
```
This will take over your terminal window and start printing logs.

From there, determine what application you need to work on and `cd` into the appropriate folder and run.

```bash
docker-compose up
```

Great! We now have an app and the API running. You can visit the app your working on by going to the url defined in the `docker-compose.yaml` file with the key `VIRTUAL_HOST`.

### Helpful tips

#### Multiple Applications Running at One:

You can have as many applications as you'd like running at one time. Every single one of our apps has a `docker-compose.yaml` file in the root. So, it should be as easy as navigating to the desired app's root directory and running
```bash
docker-compose up
```

#### App Specific Command Line Tools

Other than the tools in Step 1 we don't want you to have to install anything in the command line. However, you'll often need to run tools like `phpunit` or `yarn`. These tools are generally built into the `docker` containers in the `docker-compose.yaml` file. You can run the tools using `docker-compose exec [container-name] [command]`. In many of our apps we have a container named `utility` meant for cases like this. For example, to migrate the database in the Laravel API you can run this command in the root of the directory `docker-compose exec utility php artisan migrate`
